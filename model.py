import torch
import torch.nn as nn
import torch.nn.functional as f

from torchtrain import DeepModel, Settings, HParams


class ConvNet(DeepModel):
    __scope__ = "model.convnet"

    def __init__(self, **kwargs):
        super(ConvNet, self).__init__(**kwargs)

        with self.scope() as p:
            self.output_size = p.midi_pitch_max - p.midi_pitch_min + 1

            self.conv = nn.Sequential(
                nn.Conv2d(1, 48, 3, padding=1),
                nn.BatchNorm2d(48),
                nn.ReLU(),
                nn.Conv2d(48, 48, 3, padding=1),
                nn.BatchNorm2d(48),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
                nn.Conv2d(48, 96, 3, padding=1),
                nn.BatchNorm2d(96),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
            )
            # nn.Flatten(),
            self.fc = nn.Sequential(
                nn.Dropout(0.5),
                nn.Linear(p.freq_bins // 4 * p.time_bins * 96, 768),
                nn.ReLU(),
                nn.Linear(768, self.output_size),
                nn.Sigmoid(),
            )

    def forward(self, x):
        bs, channels, time_bins, freq_bins = x.shape

        if time_bins == 5:
            conv = self.conv(x)
            output = self.fc(conv.view(bs, -1))
            return output
        elif time_bins > 5:
            outputs = []
            for i in range(time_bins - 4):
                conv = self.conv(x[:, :, i : i + 5, :])
                output = self.fc(conv.reshape(bs, -1))
                outputs.append(output.view((1, 1, -1)))
            outputs = torch.cat(outputs, 1)
            return outputs
        else:
            raise Exception("need at least 5 time bins")

with Settings.default.scope(ConvNet.__scope__) as hparams:
    hparams.time_bins = 5
    hparams.freq_bins = 229
    hparams.midi_pitch_min = 21
    hparams.midi_pitch_max = 108
