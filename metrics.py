import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np

from torchtrain import Metric

from typing import Any
import os


class LossMetric(Metric):
    __name__ = "loss"

    def __init__(self, **kwargs):
        super(LossMetric, self).__init__(**kwargs)

    def init(self) -> None:
        self.loss_count = 0
        self.loss_sum = 0
        self.last_loss = 0

    def collect(self, batch_data: dict) -> None:
        assert "loss" in batch_data

        self.last_loss = batch_data["loss"]
        self.loss_count += 1
        self.loss_sum += self.last_loss

    def calculate(self) -> None:
        self.append(self.loss_sum / self.loss_count)

    def summarize(self) -> str:
        return f"loss: {self.get_last().item():.5f}"

    def benchmark(self) -> Any:
        return False

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        writer.add_scalar(
            os.path.join(tag_prefix, self.name), self.metrics[-1], global_step=step
        )

    def preview(self) -> Any:
        return f"loss: {self.last_loss.item():.5f}"


class ClassificationMetric(Metric):
    __name__ = "classification"

    def __init__(self, **kwargs):
        super(ClassificationMetric, self).__init__(**kwargs)

        self.num_class = kwargs.get("num_class", 1)
        self.min_pitch = kwargs.get("min_pitch", 0)

    def init(self) -> None:
        self.tp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.tn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.freq = torch.zeros(self.num_class, dtype=int, device=self.device)

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "prediction" in batch_data and "ground_truth" in batch_data

        pred = batch_data["prediction"] >= 0.5
        gt = batch_data["ground_truth"].bool()

        self.tp += (pred & gt).sum(dim=0)
        self.tn += ((~pred) & (~gt)).sum(dim=0)
        self.fp += (pred & (~gt)).sum(dim=0)
        self.fn += ((~pred) & gt).sum(dim=0)
        self.freq += gt.sum(dim=0)

    def calculate(self) -> None:
        accuracy = (self.tp + self.tn).float() / (
            self.tp + self.tn + self.fp + self.fn
        ).float()
        precision = self.tp.float() / (self.tp + self.fp).float()
        recall = self.tp.float() / (self.tp + self.fn).float()
        f1 = 2 * (precision * recall) / (precision + recall)

        accuracy[accuracy != accuracy] = 0
        precision[precision != precision] = 0
        recall[recall != recall] = 0
        f1[f1 != f1] = 0

        self.append(torch.cat([self.freq.float(), accuracy, precision, recall, f1]))

    def summarize(self) -> str:
        [freq, acc, pre, rec, f1] = self.get_last().view(5, -1)
        return " ".join(
            [
                f"imb: 1:{freq.max().float()/freq[freq!=0].min().float():.1f}",
                f"acc: {acc.mean()*100:.2f}±{acc.std()*100:.2f}%",
                f"pre: {pre.mean()*100:.2f}±{pre.std()*100:.2f}%",
                f"rec: {rec.mean()*100:.2f}±{rec.std()*100:.2f}%",
                f"f1: {f1.mean()*100:.2f}±{f1.std()*100:.2f}%",
            ]
        )

    def benchmark(self) -> Any:
        [_, acc, pre, rec, f1] = self.get_last().view(5, -1)
        return {
            "accuracy/mean": acc.mean(),
            "accuracy/std": acc.std(),
            "precision/mean": pre.mean(),
            "precision/std": pre.std(),
            "recall/mean": rec.mean(),
            "recall/std": rec.std(),
            "f1/mean": f1.mean(),
            "f1/std": f1.std(),
        }

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        from mpl_toolkits.axes_grid1 import host_subplot
        from matplotlib.ticker import FuncFormatter
        import mpl_toolkits.axisartist as AA
        import matplotlib.pyplot as plt

        [freq, acc, pre, rec, f1] = self.get_last().view(5, -1)

        freq = freq.clone().detach().cpu()
        acc = acc.clone().detach().cpu()
        pre = pre.clone().detach().cpu()
        rec = rec.clone().detach().cpu()
        f1 = f1.clone().detach().cpu()
        notes_index = np.arange(
            self.min_pitch, self.min_pitch + freq.shape[0], dtype=int
        )

        fig = plt.gcf()
        ax = host_subplot(111, axes_class=AA.Axes)
        ax_alt = ax.twinx()

        ax_alt.plot(notes_index, freq.numpy(), label="frequency")
        ax_alt.set_yscale("log")

        ax.scatter(notes_index, acc.numpy(), label="accuracy")
        ax.scatter(notes_index, pre.numpy(), label="precision")
        ax.scatter(notes_index, rec.numpy(), label="recall")
        ax.scatter(notes_index, f1.numpy(), label="f1")

        def notes(x, pos):
            notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
            return notes[int(x) % 12] + str((int(x) // 12) - 1)

        formatterx = FuncFormatter(notes)
        ax.xaxis.set_major_formatter(formatterx)

        ax.set_xlabel("notes")
        ax.set_ylabel("pourcentage")
        ax_alt.set_ylabel("frequency of occurrence")
        ax.legend()
        fig.tight_layout()

        writer.add_figure(os.path.join(tag_prefix, self.name), fig, global_step=step)
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "accuracy"),
            acc.mean(),
            global_step=step,
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "precision"),
            pre.mean(),
            global_step=step,
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "recall"), rec.mean(), global_step=step
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "f1"), f1.mean(), global_step=step
        )
