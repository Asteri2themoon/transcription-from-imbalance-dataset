import torch
from torch.utils.data import DataLoader
import torchvision.datasets as datasets
import numpy as np
import zipfile

import matplotlib.pyplot as plt

import os
import glob
import json


def load_spec(file_name, spec_type):
    try:
        data = np.load(file_name)
    except zipfile.BadZipFile as e:
        print(f"Error BadZipFile with file {file_name}: {e}")
        raise Exception(f"Error BadZipFile with file {file_name}: {e}")

    x = data[spec_type]
    vel = data["vel"]
    y = vel[0, :, vel.shape[2] // 2]

    x = torch.t(torch.from_numpy(x).float())
    y = torch.t(torch.from_numpy(y).float())

    freq_bins, time_bins = x.shape
    x = x.view((1, freq_bins, time_bins))

    return x, y


def load_mel(file_name):
    return load_spec(file_name, "mel")


def load_cqt(file_name):
    return load_spec(file_name, "cqt")


def load_hybrid_cqt(file_name):
    return load_spec(file_name, "hybrid_cqt")


def get_dataloader(
    config_file: str,
    batch_size: int,
    spec_type: str = "mel",
    num_workers: str = 8,
    shuffle: bool = True,
):
    assert spec_type in ["mel", "cqt", "hybrid_cqt"]
    assert batch_size > 0
    assert num_workers >= 0

    loader = None
    if spec_type == "mel":
        loader = load_mel
    elif spec_type == "cqt":
        loader = load_cqt
    elif spec_type == "hybrid_cqt":
        loader = load_hybrid_cqt

    with open(config_file, "r") as f:
        config = json.load(f)

    dataset_train = datasets.DatasetFolder(
        root=config["train"], loader=loader, extensions=(".npz",)
    )
    dataset_validate = datasets.DatasetFolder(
        root=config["validate"], loader=loader, extensions=(".npz",)
    )
    dataset_test = datasets.DatasetFolder(
        root=config["test"], loader=loader, extensions=(".npz",)
    )
    dataloader_train = DataLoader(
        dataset_train, batch_size=batch_size, num_workers=num_workers, shuffle=shuffle
    )
    dataset_validate = DataLoader(
        dataset_train, batch_size=batch_size, num_workers=num_workers, shuffle=shuffle
    )
    dataset_test = DataLoader(
        dataset_train, batch_size=batch_size, num_workers=num_workers, shuffle=shuffle
    )
    return dataloader_train, dataset_validate, dataset_test
