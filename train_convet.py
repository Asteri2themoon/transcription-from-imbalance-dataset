import torch
import torch.optim as optim
import torch.nn as nn
import pandas as pd
import numpy as np

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

from torchtrain import Training, Settings, HParams

from metrics import LossMetric, ClassificationMetric
from model import ConvNet
from loss import BLNLoss
from dataset import get_dataloader

import os
import math
import time


class TrainConvNet(Training):
    __scope__ = "training.convnet"

    def __init__(self, **kwargs):
        super(TrainConvNet, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            ClassificationMetric(
                num_class=88,
                min_pitch=self.hparams.model.convnet.midi_pitch_min,
                device=self.device,
            ),
        )

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            self.epoch = p.epoch

        self.model = ConvNet(**hparams.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.optimizer = optim.SGD(
                self.model.parameters(),
                lr=p.lr,
                momentum=p.momentum,
                weight_decay=p.decay,
            )
            self.loss = BLNLoss(eta=p.eta, k=p.k, multiclass=True)

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            (
                self.train_loader,
                self.validation_loader,
                self.test_loader,
            ) = get_dataloader(
                config_file=dataset_path,
                batch_size=self.batch_size,
                spec_type="mel",
                num_workers=self.num_workers,
                shuffle=True,
            )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer.zero_grad()

        x, a = data[0]
        x = x.to(self.device)
        a = a.to(self.device).ceil()
        pred = model(x)
        l, _, _ = loss(pred, a)

        l.backward()
        optimizer.step()

        res = {"loss": l, "prediction": pred, "ground_truth": a}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        x, a = data[0]
        x = x.to(self.device)
        a = a.to(self.device).ceil()
        pred = model(x)
        l, _, _ = loss(pred, a)

        return {"loss": l, "prediction": pred, "ground_truth": a}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        x, a = data[0]
        x = x.to(self.device)
        a = a.to(self.device).ceil()
        pred = model(x)
        l, _, _ = loss(pred, a)

        return {"loss": l, "prediction": pred, "ground_truth": a}


with Settings.default.scope(TrainConvNet.__scope__) as hparams:
    hparams.batch_size = 256
    hparams.epoch = 200
    hparams.lr = 1e-1
    hparams.decay = 5e-4
    hparams.momentum = 0.9
    hparams.eta = 0.01
    hparams.k = 10
